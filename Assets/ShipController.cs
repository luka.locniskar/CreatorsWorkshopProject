﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour {
	float factor = 2f;
	float forwardSpeed = 20f;
	Rigidbody myRigidbody;
	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody> ();
		//Cursor.lockState = CursorLockMode.Locked;
		Screen.lockCursor = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float h = Input.GetAxis("Mouse X");
		float v = Input.GetAxis ("Mouse Y");
		Debug.Log ("h= "+h + "v= "+v);
		float r = 0;
		if(Input.GetKey(KeyCode.Q))
		{
			r = 1;
		}
		else if(Input.GetKey(KeyCode.E))
		{
			r = -1;	
		}
		transform.Rotate(new Vector3 (-v *factor, h *factor, r*factor));
		if (Input.GetKey (KeyCode.W)) 
		{
			myRigidbody.AddForce (transform.forward * forwardSpeed, ForceMode.Acceleration);
		}
		if (Input.GetKey (KeyCode.S)) 
		{
			myRigidbody.AddForce (-transform.forward * forwardSpeed, ForceMode.Acceleration);
		}
		if (Input.GetKey (KeyCode.C)) 
		{
			myRigidbody.AddForce (Vector3.down * forwardSpeed, ForceMode.Acceleration);
		}
		if (Input.GetKey (KeyCode.Space)) 
		{
			myRigidbody.AddForce (Vector3.up * forwardSpeed, ForceMode.Acceleration);
		}
	}
}
